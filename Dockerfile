FROM eclipse-temurin:11-jre-alpine

LABEL io.c12.DevName="Bala"
ENV BASE_JAVA_OPTS="-Duser.timezone=America/Chicago"

# add local user and app directory
RUN addgroup -S appuser && adduser -D -u 1001 -g "" -S -G appuser appuser

# copy war file and entrypoint
WORKDIR /app
COPY --chown=appuser:appuser /build/libs/sb-app-*.jar ./app.jar

USER appuser

CMD java ${JAVA_OPTS} ${BASE_JAVA_OPTS} -jar app.jar
