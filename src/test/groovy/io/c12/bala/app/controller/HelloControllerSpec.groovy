package io.c12.bala.app.controller

import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class HelloControllerSpec extends Specification {

    HelloController helloController = new HelloController()
    MockMvc mockMvc = MockMvcBuilders.standaloneSetup(helloController).build()

    def "say hello test"() {
        when: "controller is invoked"
        def result = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/hello")
            .contentType(MediaType.APPLICATION_JSON))

        then: "verify response from controller"
        result.andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print())
        result.andExpect(MockMvcResultMatchers.jsonPath("\$.status").value("success"))
    }


}
