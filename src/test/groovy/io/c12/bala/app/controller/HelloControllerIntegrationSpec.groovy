package io.c12.bala.app.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(controllers = [HelloController])
class HelloControllerIntegrationSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    def "say hello controller test"() {
        when: "Controller method is called"
        def result = mockMvc.perform(get("/api/v1/hello")
            .contentType(MediaType.APPLICATION_JSON))

        then: "verify results"
        result.andExpect(status().isOk()).andDo(print())
        result.andExpect(jsonPath("\$.status").isNotEmpty())
        result.andExpect(jsonPath("\$.status").value("success"))
    }
}
